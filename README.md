This is a script which uses the Fedora Calendar ("fedocal") API to read
upcoming meetings and write them to given topics.

As currently implemented, it gathers meetings in the one week period
starting three weeks from Monday of this week, and writes posts for
those. **It does not attempt to update them if anything changes**, and
**it does not look for new meetings in the upcoming three weeks**.

It would be lovely if it could be made smarter and more capable.
PRs welcome. :)
