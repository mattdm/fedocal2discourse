#!/usr/bin/python

import requests
from datetime import datetime, timedelta

start = datetime.utcnow() + timedelta(weeks=3)
end = start+timedelta(weeks=1)
endpoint = "https://calendar.fedoraproject.org/api/meetings/"
calendar = 'council'

r = requests.get(f"{endpoint}?calendar={calendar}"
                 f"&start={start.strftime('%Y-%m-%d')}"
                 f"&end={end.strftime('%Y-%m-%d')}")
if r.status_code != 200:
    r.raise_for_status()


for meeting in r.json()['meetings']:
    msg = (f"{meeting['meeting_name']}\n"
           f"[date={meeting['meeting_date']}"
           f" time={meeting['meeting_time_start']}"
           f" timezone=\"{meeting['meeting_timezone']}\"]"
           f" → "
           f"[date={meeting['meeting_date_end']}"
           f" time={meeting['meeting_time_stop']}"
           f" timezone=\"{meeting['meeting_timezone']}\"]"
           f"\n\n"
           f"{meeting['meeting_information']}"
           f"\n\n"
           f"<small>"
           f"via [fedocal2discourse](https://pagure.io/fedocal2discourse])"
           f" _meeting id: [{meeting['meeting_id']}]"
           f"(https://calendar.fedoraproject.org/meeting/{meeting['meeting_id']}/)_"
           f"</small>"
           )

    print(msg)
